import React, {Fragment, useState} from 'react';
import Formulario from './components/formulario';
import Cita from './components/Cita';

function App() {

  let inicio = JSON.parse(localStorage.getItem('citas'));
  //Arreglo de citas
  const [citas, guardarCitas] = useState([]);

  //funcion que elimina cita x su ID
  const eliminarCita = id => {
    const nuevasCitas = citas.filter(cita => cita.id !== id);
    guardarCitas(nuevasCitas);
  }

  //Funcion tome citas actuales y tome las nuevas
  const crearCita = cita =>{
    guardarCitas([
      ...citas,
      cita
    ])
    
  }

  //mensaje condicional
  const titulo = citas.length === 0 ? 'No hay citas' : 'Administra tus citas'
  return (
    <Fragment>
      <h1>Administrador de Pacientes</h1>
      <div className="container">
        <div className="row">
          <div className="one-half column">
            <Formulario crearCita = {crearCita}/>
          </div>
          <div className="one-half column">
            <h2>{titulo}</h2>
            {citas.map(cita => (
             <Cita
             key = {cita.id} 
             cita={cita}
             eliminarCita = {eliminarCita}
             />
            ))}
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default App;
