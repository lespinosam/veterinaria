import React, { Fragment, useState } from 'react';
import uuid from 'uuid/v4';

const Formulario = ({crearCita}) => {

    // Crear states de citas
    const [cita, actualizarCita] = useState({
            mascota:'',
            propietario:'',
            fecha:'',
            hora:'',
            sintomas:''
    })

    //State para control de errores
    const [error, actualizaError] = useState(false)

    //funcion que se ejcuta cada vex que el usuario escribe en input
    const actualizaState = event => {
        actualizarCita({
            ...cita,
            [event.target.name] : event.target.value
        })
    
    }
    const {mascota, propietario, fecha, hora, sintomas} = cita;
    const submitCita = e =>{
        e.preventDefault();
        
        //validacion de formulario
        if(mascota.trim() === '' || propietario.trim() === '' || fecha.trim()=== '' || hora.trim() === '' || sintomas.trim() === ''){
            actualizaError(true);
            return;
        }

        //Eliminar mensaje de error
        actualizaError(false);

        // Asignar ID
        cita.id = uuid();
        
        //Agregar citas
        crearCita(cita)
        //Limpiar formulario
        actualizarCita({
            mascota:'',
            propietario:'',
            fecha:'',
            hora:'',
            sintomas:''
        })
    }

    

    return (
        <Fragment>
            <h2>Crear Cita</h2>
            {error ? <p className="alerta-error"> Todos los datos son obligatorio</p> : null}
            <form
               onSubmit={submitCita}
            >
                <label>Nombre Mascota</label>
                <input 
                    type="text"
                    name="mascota"
                    className="u-full-width"
                    placeholder="Nombre Mascota"  
                    onChange={actualizaState}   
                    value={mascota}
                />
                <label>Nombre Dueño</label>
                <input 
                    type="text"
                    name="propietario"
                    className="u-full-width"
                    placeholder="Nombre Dueño de la Mascota"   
                    onChange={actualizaState}     
                    value={propietario}
                />
                <label>Fecha</label>
                <input 
                    type="date"
                    name="fecha"
                    className="u-full-width"
                    onChange={actualizaState}   
                    value={fecha}
                />
                <label>Hora</label>
                <input 
                    type="time"
                    name="hora"
                    className="u-full-width"
                    onChange={actualizaState}   
                    value={hora}
                />
                <label>Descripcion</label>
                <textarea
                    className="u-full-width"
                    name ="sintomas"
                    onChange={actualizaState}   
                    value={sintomas}
                >
                </textarea> 
                <button
                    type="submit"
                    className ="u-full-width button-primary"
                >Agregar cita
                </button>
            </form>
        </Fragment>
    );
};

export default Formulario;